drumgizmo for Debian
--------------------

 * Please note that for using DrumGizmo you will need to download some
   compatible drum kit audio data made by the community (such as those
   available at www.drumgizmo.org) or create one yourself with DGEdit
   (provided in the dgedit package).

 * SSE optimization is only enabled on amd64, where it is part of the
   base architecture. The best way to approach enabling SSE would be for
   upstream to detect it in compile time.

 * The *.la files have been deleted, as per Debian Policy 3.9.1.0.

 * Compiled without VST support (since the VST SDK isn't packaged).

 * Upstream uses their own built-in minimalistic png decoder,
   plugingui/lodepng, instead of libpng, because if DrumGizmo uses a libpng
   with different version than Ardour it will crash at runtime.
   I have left upstream's lodepng in place, so Debian's DrumGizmo will work
   with whatever Ardour version (Debian's, upstream's paid, old, etc).

 * dgreftest binary runs a known session of drumgizmo in a controlled
   environment and should produce a bitwise equal output compared to a previous
   run. It is used for detecting changes when refactoring code. It is not useful
   for end users, and therefore is not being packaged.

 -- Víctor Cuadrado Juan <me@viccuad.me>, Sun, 30 Oct 2016 13:14:15 +0100
