/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            midievent.h
 *
 *  Sun Feb  7 15:09:01 CET 2016
 *  Copyright 2016 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of PluginGizmo.
 *
 *  PluginGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PluginGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with PluginGizmo; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#pragma once

#include <stdint.h>
#include <stdio.h>

#include <vector>

enum class MidiEventType
{
	Unknown,
	NoteOn,
	NoteOff,
	Aftertouch,
};

class MidiEvent
{
public:
	MidiEvent() = default;
	MidiEvent(int64_t time, const char* data, std::size_t size);

	int64_t getTime() const;
	const char* getData() const;
	std::size_t getSize() const;

	MidiEventType type{MidiEventType::Unknown};
	int key{0};
	int velocity{0};

private:
	int64_t time;
	std::vector<char> data;
};
