/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            midievent.cc
 *
 *  Sun Feb  7 15:09:01 CET 2016
 *  Copyright 2016 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of PluginGizmo.
 *
 *  PluginGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PluginGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with PluginGizmo; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#include "midievent.h"

#include <iostream>

MidiEvent::MidiEvent(int64_t time, const char* data, std::size_t size)
	: time(time)
{
	//std::cout << __PRETTY_FUNCTION__ <<
	//	" data: " << (void*)data <<
	//	" size: " << size <<
	//	std::endl;

	this->data.resize(size);
	for(std::size_t i = 0; i < size; ++i)
	{
		this->data[i] = data[i];
	}

	if((data[0] & 0xF0) == 0x80) // note off
	{
		type = MidiEventType::NoteOff;
		key = data[1];
		velocity = data[2];
	}

	if((data[0] & 0xF0) == 0x90) // note on
	{
		type = MidiEventType::NoteOn;
		key = data[1];
		velocity = data[2];
	}

	if((data[0] & 0xF0) == 0xA0) // aftertouch
	{
		type = MidiEventType::Aftertouch;
		key = data[1];
		velocity = data[2];
	}
}

int64_t MidiEvent::getTime() const
{
	return time;
}

const char* MidiEvent::getData() const
{
	return data.data();
}

std::size_t MidiEvent::getSize() const
{
	return data.size();
}
