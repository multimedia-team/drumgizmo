/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            pluginvst.h
 *
 *  Mon Feb  8 19:24:39 CET 2016
 *  Copyright 2016 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of PluginGizmo.
 *
 *  PluginGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PluginGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with PluginGizmo; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#pragma once

#include <memory>

#include <plugin.h>

#include <public.sdk/source/vst2.x/audioeffectx.h>
#include <public.sdk/source/vst2.x/aeffeditor.h>

#if defined(WIN32)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif // defined(WIN32)

class PluginVST
	: public Plugin
	, public AudioEffectX
{
public:
	//! Call this to set up number of inputs/outpus, unique id etc...
	//! IMPORTANT: This must be called form the constructor.
	void init() override;

	//! Get current free-wheel mode.
	bool getFreeWheel() const override;


	//! Call this to get current samplerate.
	float getSamplerate() override;

	//! This method is called by the host when the free-wheel mode changes.
	virtual void onSamplerateChange(float samplerate) override = 0;


	//! Call this to get current frame-size.
	std::size_t getFramesize() override;

	//! This method is called by the host when the frame-size changes.
	virtual void onFramesizeChange(std::size_t framesize) override = 0;


	//! Call this to get current active state
	bool getActive() override;

	//! This method is called by the host when the active state changes.
	virtual void onActiveChange(bool active) override = 0;


	//! This method is called by the host to get the current state for storing.
	virtual std::string onStateSave() override = 0;

	//! This method is called by the host when a new state has been loaded.
	virtual void onStateRestore(const std::string& config) override = 0;


	//! This is method is called by the host to get the current latency.
	//! \param The latency in samples.
	float getLatency() override;

	//! Call this method to signal a latency change to the host.
	//! \param latency The latency in samples.
	void setLatency(float latency) override;


	//! Called by the the host to get the number of audio input channels.
	//! This must remain constant during the lifespan of the plugin instance.
	virtual std::size_t getNumberOfAudioInputs() override = 0;

	//! Called by the the host to get the number of audio output channels.
	//! This must remain constant during the lifespan of the plugin instance.
	virtual std::size_t getNumberOfAudioOutputs() override = 0;


	//! Called by the the host to get the number of midi input channels.
	//! This must remain constant during the lifespan of the plugin instance.
	virtual std::size_t getNumberOfMidiInputs() override = 0;

	//! Called by the the host to get the number of midi output channels.
	//! This must remain constant during the lifespan of the plugin instance.
	virtual std::size_t getNumberOfMidiOutputs() override = 0;



	// VST plugin information

	//! Get unique plugin id.
	std::string getId() override = 0;

	// Functions used to set plugin information for VST

	//! Returns value which is then used by getEffectName
	std::string getEffectName() override = 0;
	//! Returns value which is then used by getVendorString
	std::string getVendorString() override = 0;
	//! Returns value which is then used by getProductString
	std::string getProductString() override = 0;
	//! Returns value which is then used by getPlugCategory
	PluginCategory getPluginCategory() override = 0;

	//! Fill \e text with a string identifying the effect
	bool getEffectName(char* name) override;

	//! Fill \e text with a string identifying the vendor
	bool getVendorString(char* text) override;

	//! Fill \e text with a string identifying the product name
	bool getProductString(char* text) override;

	//! Specify a category that fits the plug (#VstPlugCategory)
	virtual VstPlugCategory getPlugCategory() override;


	virtual void process(std::size_t pos,
	                     const std::vector<MidiEvent>& input_events,
	                     std::vector<MidiEvent>& output_events,
	                     const std::vector<const float*>& input_samples,
	                     const std::vector<float*>& output_samples,
	                     std::size_t count) override = 0;

	//
	// GUI
	//
	//! Return true if a GUI implementation is to be used.
	virtual bool hasGUI() override
	{
		return false;
	}

	//! Create new window.
	virtual void* createWindow(void *parent) override { return nullptr; }

	//! Destroy window.
	virtual void onDestroyWindow() override {}

	//! Show window.
	virtual void onShowWindow() override {}

	//! Hide window.
	virtual void onHideWindow() override {}

	//! Called regularly by host; process ui events.
	virtual void onIdle() override {}

	//! Signal new window size to host.
	void resizeWindow(std::size_t width, std::size_t height) override;

	//! Signal close window event to the host.
	void closeWindow() override;

protected:
	bool active{false};

	void updateLatency();
	float current_latency{0.0f};
	float update_latency{0.0f};

	bool free_wheel{true};

	std::vector<MidiEvent> input_events;

	std::size_t pos{0};

public:
	PluginVST(audioMasterCallback audioMaster);
	virtual ~PluginVST();

	// From AudioEffect:
	void open() override;
	void close() override;
	void suspend() override;
	void resume() override;
	bool getInputProperties(VstInt32 index, VstPinProperties* props) override;
	bool getOutputProperties(VstInt32 index, VstPinProperties* props) override;

	// Callbacks:
	void setSampleRate(float sampleRate) override;
	void setBlockSize(VstInt32 blockSize) override;

	VstInt32 getChunk(void **data, bool isPreset) override;
	VstInt32 setChunk(void *data, VstInt32 byteSize, bool isPreset) override;

	// From AudioEffectX:
	VstInt32 canDo(char* text) override;

	void processReplacing(float** inputs, float** outputs,
	                      VstInt32 sampleFrames) override;
	VstInt32 processEvents(VstEvents *events) override;

	// UI
	class UI :
		public AEffEditor
	{
	public:
		UI(PluginVST& plugin_vst);

		bool open(void* ptr) override;
		void close() override;
		bool isOpen() override;
		void idle() override;
		bool getRect(ERect** rect) override;

		PluginVST& plugin_vst;
		bool is_open{false};

		ERect rect{0,0,100,100};
	};

private:
	std::shared_ptr<UI> editor;
};

AudioEffect* createEffectInstance(audioMasterCallback audioMaster);
